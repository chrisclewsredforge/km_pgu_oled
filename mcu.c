/*****************************************************************************
      Red Forge Ltd
      Alders Drive
      Moons Moat East
      Redditch
      Worcs
      England
      B98 0RF

      Title:          MCU register configurations for RCU
      Filename:       MCU.c
      Issue:          V1.0
      Written by:     Tommy Liu
      Modified by:
      Date:           26 Jan 2004
      Compiler:       CCS PCWH V3.158

      Red Forge Ltd owns the copyright in this document and associated
      documents and all rights are reserved.  This document and associated
      documents must not be used for any purpose other than that for which
      they are supplied and must not be copied in whole or in part, or
      disclosed to others without the prior written consent of Red Forge Ltd.
      Any copy of this document made by any method must also contain a copy
      of this legend.

               (c) Red Forge Ltd. 2004

--------------------------------------------------------------------------------

      CHANGE HISTORY
      Issue   Author      Date
      See rcu.c

*******************************************************************************/
//--------------------------------------------------------------------------
// Module:  TIMER3 ISR
//--------------------------------------------------------------------------
// Purpose: Increment counter for every interrupt (i.e. every 500ms)
//
// Parameters: None
//
// Returns:    None
//
//--------------------------------------------------------------------------
#int_TIMER3
void Timer3_ISR(void)
{
   set_timer3(T500msValue);  // Reload Timer3

   // This counter is monitored in delay_check_OK_PB()
   timer3_overflow_startup++;
}


//--------------------------------------------------------------------------
//	Module:  setup_ports
//--------------------------------------------------------------------------
// Purpose: Configure MCU ports as inputs and outputs.
//
// Parameters:	None
//
// Returns:    None
//
//--------------------------------------------------------------------------
#separate
void setup_ports(void)
{
   set_tris_a(0b00001111);          // Port A:  A4 to A7 = outputs
                                    //          A0 to A3 = inputs

   set_tris_b(0b11000101);          // Port B:  B0, B2 to B7 = inputs
                                    //          B1 = output

   set_tris_c(0b10000000);          // Port C:  C0 to C6 outputs, C7 = input

}

//--------------------------------------------------------------------------
//	Module:  configure_ADC_ports
//--------------------------------------------------------------------------
// Purpose: Configure ADC ports.
//
// Parameters:	None
//
// Returns:    None
//
//--------------------------------------------------------------------------
#separate
void configure_ADC_ports(void)
{
   //A0 is analog input connected to sensor
   //A1 is analog input (12V Battery input with resistor network)
   //A2 ADC voltage ref 0V input
   //A3 ADC voltage ref 5V precision input

   setup_adc_ports(AN0_TO_AN1 | VREF_VREF);
   delay_ms(1);

   // According Table 19-1 in the MCU datasheet, at 4MHz, the AD clock source
   // chosen must ensure that TAD must be at least 0.8us.
   // Also, cannot use ADC_CLOCK_INTERNAL option because 4MHz is greater
   // than 1MHz unless sleep mode is used.
   // Use ADC_CLOCK_DIV_4: 4MHz/4=1MHz, T=1/1MHz=1us which is greater than 0.7us
   // Tad is the time required to produce 1 bit of conversion.

   // According to Table 26-25 in the MCU datsheet, Tcnv=12*Tad max.
   // Therefore, use ADC_TAD_MUL_16 (where 16 comes from 12+extra),
   // where extra=4 to be safe.
   setup_adc(ADC_CLOCK_DIV_4 | ADC_TAD_MUL_16);
   delay_ms(1);
}


//--------------------------------------------------------------------------
// Module:   write_defaults
//--------------------------------------------------------------------------
// Purpose:    Write defaults to EEPROM locations.
//
// Parameters: None
//
// Returns:    None
//
//--------------------------------------------------------------------------
#separate
void write_defaults (void)
{
   U8 addr;
   U8 tmp1, tmp2, tmp3;

   // If the 3 allocated locations of the EEPROM are 0xFF (unprogrammed)
   // then write defaults to EEPROM.
   tmp1=read_eeprom(0x000);
   tmp2=read_eeprom(0x001);
   tmp3=read_eeprom(0x002);
   if(tmp1==0xFF && tmp2==0xFF && tmp3==0xFF)
   {
      lcd_putc("\f PLEASE WAIT...");

      // Write to 0x1F locations
      disable_interrupts(GLOBAL);
      for(addr=0; addr<=0x1F; addr++)
      {
         write_eeprom(addr, 0x00);
      }

      write_eeprom(NUMPINS_ADDR, INITIALNUMPINS);
      write_eeprom(NUMSTONES_ADDR, MAXNUMSTONES);
      enable_interrupts(GLOBAL);

      lcd_putc("\f  ALL CLEARED");
      delay_ms(2000);
   }
}


//--------------------------------------------------------------------------
//	Module:  read_eeprom_data
//--------------------------------------------------------------------------
// Purpose: read eeprom data.
//
// Parameters:	None
//
// Returns:    None
//
//--------------------------------------------------------------------------
#separate
void read_eeprom_data(void)
{
   // Read EEPROM
   numPINs = read_eeprom(NUMPINS_ADDR);
   numStones = read_eeprom(NUMSTONES_ADDR);

   totalNumStonesUsed_hi = read_eeprom(TOTALNUMSTONESUSED_HI_ADDR);
   totalNumStonesUsed_md = read_eeprom(TOTALNUMSTONESUSED_MD_ADDR);
   totalNumStonesUsed_nd = read_eeprom(TOTALNUMSTONESUSED_ND_ADDR);
   totalNumStonesUsed_lo = read_eeprom(TOTALNUMSTONESUSED_LO_ADDR);
   totalNumStonesUsed=join4bytes(totalNumStonesUsed_hi,
     totalNumStonesUsed_md, totalNumStonesUsed_nd, totalNumStonesUsed_lo);

   totalNumPINsUsed_hi = read_eeprom(TOTALNUMPINSUSED_HI_ADDR);
   totalNumPINsUsed_md = read_eeprom(TOTALNUMPINSUSED_MD_ADDR);
   totalNumPINsUsed_nd = read_eeprom(TOTALNUMPINSUSED_ND_ADDR);
   totalNumPINsUsed_lo = read_eeprom(TOTALNUMPINSUSED_LO_ADDR);
   totalNumPINsUsed=join4bytes(totalNumPINsUsed_hi,
     totalNumPINsUsed_md, totalNumPINsUsed_nd, totalNumPINsUsed_lo);
}


//--------------------------------------------------------------------------
// Module:  splitU32Into4
//--------------------------------------------------------------------------
// Purpose: Split U32 into 4 separate U8 variables.
//
// Parameters: *ptrIn: pointer to U16 input
//             *ptrHi: pointer to U8 output Hi
//             *ptrMd: pointer to U8 output Md
//             *ptrNd: pointer to U8 output Nd
//             *ptrLO: pointer to U8 output Lo
//
// Returns:    None
//
//--------------------------------------------------------------------------
#separate
void splitU32Into4 (U32 *ptrIn, U8 *ptrHi, U8 *ptrMd, U8 *ptrNd, U8 *ptrLo)
{
   U32 tmp=0;

   *ptrHi = *ptrIn/1E6;

   tmp = *ptrIn/1E4;  *ptrMd = tmp%100;

   tmp = *ptrIn/100;  *ptrNd = tmp%100;

   *ptrLo = *ptrIn%100;
}


//--------------------------------------------------------------------------
// Module:  join4bytes
//--------------------------------------------------------------------------
// Purpose:    The values of high, mid1, mid2 and low bytes
//             are combined to give an 8 digit value.
//
// Parameters: hi(x1E6), md(x1E4), nd(x1E2) and lo(x1)
//
// Returns:    8-digit value
//
//--------------------------------------------------------------------------

#separate
U32 join4bytes (U8 hi, U8 md, U8 nd, U8 lo)
{
   return( (U32)hi*1E6 + (U32)md*1E4 + (U32)nd*1E2 + lo );
}
