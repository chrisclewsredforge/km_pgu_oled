/*****************************************************************************
      Red Forge Ltd
      9, Palmers Road
      Moons Moat East
      Redditch
      Worcs
      England
      B98 0RF

      Title:          PGXU misc routines
      Filename:       pgxu_misc.c
      Issue:          V2.0
      Written by:     Tommy Liu
      Modified by:
      Date:           11 Mar 2010
      Compiler:       CCS PCWH V3.249

      Red Forge Ltd owns the copyright in this document and associated
      documents and all rights are reserved.  This document and associated
      documents must not be used for any purpose other than that for which
      they are supplied and must not be copied in whole or in part, or
      disclosed to others without the prior written consent of Red Forge Ltd.
      Any copy of this document made by any method must also contain a copy
      of this legend.

               (c) Red Forge Ltd. 2010

--------------------------------------------------------------------------------

      CHANGE HISTORY
      Issue   Author      Date

      2.0     T Liu	     11 Mar 2010
      First Issue.

*******************************************************************************/
//--------------------------------------------------------------------------
//	Module:  checkPINsEnterStone
//--------------------------------------------------------------------------
// Purpose: Check no. of PIN's left and display msg or enter stones.
//
// Parameters:	None
//
// Returns:    TOO_MANY_PINS
//             CORRECT
//             WRONG
//             N_PRESSED
//
//--------------------------------------------------------------------------
#separate
U8 checkPINsEnterStone (void)
{
   U8 retVal=0;
   U8 result=0;

   if(numPINs>THRESHNUMPINS)
   {
      lcd_putc("\f  INVALID: TOO\n MANY PINS LEFT");
      delay_ms(3000);
      retVal=TOO_MANY_PINS;
   }
   else
   {
      result == enterStoneAndDisplayAC();
      if(result == CORRECT)
      {
         retVal=CORRECT;
      }
      else if(result == WRONG)
      {
         retVal=WRONG;
      }
      else if(result == N_PRESSED)
      {
         retVal=N_PRESSED;
      }
   }

   return retVal;
}


//--------------------------------------------------------------------------
// Module:  checkStones
//--------------------------------------------------------------------------
// Purpose: Check no. of stones left and display appropriate message.
//
// Parameters: None
//
// Returns:    None
//
//--------------------------------------------------------------------------
#separate
void checkStones(void)
{
   // Check no. of stones left
   if(numStones == 0)
   {
      display_NOMORESTONES_msg();
   }

   if(numStones == 1)
   {
      lcd_putc("\f ONE STONE LEFT\n BEFORE LOCKOUT");
      delay_ms(5000);
   }
}


//--------------------------------------------------------------------------
//	Module:  displayData
//--------------------------------------------------------------------------
// Purpose: Display data.
//
// Parameters:	None
//
// Returns:    None
//
//--------------------------------------------------------------------------
#separate
void displayData(void)
{
   U8 count_F2=0;

   printf(lcd_putc,"\fTotal num stones\nused = %8lu", totalNumStonesUsed);
   delay_ms(5000);
   printf(lcd_putc,"\fTotal num PIN's \nused = %8lu", totalNumPINsUsed);
   delay_check_F2_PB(10); // Wait upto 5s
   if(PB_F2_flag)
   {
      PB_F2_flag=FALSE; // Reset flag
      timer3_overflow_startup=0; // Start from beginning
      do
      {
         do
         {
            // Wait for push button
            while( (pb=CheckIRinput())==NOCMD && timer3_overflow_startup<=TEN_SEC_TIMEOUT);  
         } while(pb!=PB_F2 && timer3_overflow_startup<=TEN_SEC_TIMEOUT);
         if(pb==PB_F2)
         {
            count_F2++;
            if(count_F2>=10)
            {
               lcd_putc("\f MORE PINS AND\n STONES ADDED");
               delay_ms(5000);
               numPINs = INITIALNUMPINS;
               numStones = MAXNUMSTONES;
               totalNumStonesUsed=0;
               totalNumPINsUsed=0;
               disable_interrupts(GLOBAL);
               write_eeprom(NUMPINS_ADDR, numPINs);
               write_eeprom(NUMSTONES_ADDR, numStones);
               write_eeprom(TOTALNUMPINSUSED_HI_ADDR, 0);
               write_eeprom(TOTALNUMPINSUSED_MD_ADDR, 0);
               write_eeprom(TOTALNUMPINSUSED_ND_ADDR, 0);
               write_eeprom(TOTALNUMPINSUSED_LO_ADDR, 0);
               write_eeprom(TOTALNUMSTONESUSED_HI_ADDR, 0);
               write_eeprom(TOTALNUMSTONESUSED_MD_ADDR, 0);
               write_eeprom(TOTALNUMSTONESUSED_ND_ADDR, 0);
               write_eeprom(TOTALNUMSTONESUSED_LO_ADDR, 0);
               enable_interrupts(GLOBAL);
            }
         }
      } while(timer3_overflow_startup<=TEN_SEC_TIMEOUT); // Timeout=10s
   }
}


//--------------------------------------------------------------------------
//	Module:     display_NOMORESTONES_msg
//--------------------------------------------------------------------------
// Purpose:    Display error message.
//             value.
//
// Parameters:	None
//
// Returns:    None
//
//--------------------------------------------------------------------------
#separate
void display_NOMORESTONES_msg (void)
{
   // Display msg forever
   lcd_send_byte(0,0x0C);     // NO blinking or underline cursor
   while(TRUE)
   {
      lcd_putc("\f NO MORE STONES\n  RETURN UNIT");
      delay_ms(2000);
      lcd_putc("\f  TO RED FORGE\n  FOR RESETTING");
      delay_ms(2000);
   }
}


//--------------------------------------------------------------------------
//	Module:     enter_AC
//--------------------------------------------------------------------------
// Purpose:    Enter 6-digit AC and convert its string into a 6 digit int32
//             value.
//
// Parameters:	None
//
// Returns:    6 digit AC in int32
//             3 = N pressed
//
//--------------------------------------------------------------------------
#separate
int32 enter_AC (void)
{
   U8 x=0;
   bool ea_flag=STAY;

   // Note: x=0 is the 1st digit of AC
   //       x=5 is the 6th digit of AC

   printf(lcd_putc,"\fSTONE: %06lu\nENTER A/C ______", random_stone);
   lcd_gotoxy(11,2);                     // Position cursor
   lcd_send_byte(0,0x0D);                // Blinking cursor

   do
   {
      ea_flag=STAY;             // Required for seed re-entry
      while( (pb=CheckIRinput()) == NOCMD); // Wait for push button press
      if(pb==PB_BACK_NO)
         return 3;
      else if( (pb>=PB_0 && pb<=PB_9) )
      {
         do
         {
            switch(pb)
            {
               case PB_0: lcd_putc("0"); str_entered_AC[x++]='0'; break;
               case PB_1: lcd_putc("1"); str_entered_AC[x++]='1'; break;
               case PB_2: lcd_putc("2"); str_entered_AC[x++]='2'; break;
               case PB_3: lcd_putc("3"); str_entered_AC[x++]='3'; break;
               case PB_4: lcd_putc("4"); str_entered_AC[x++]='4'; break;
               case PB_5: lcd_putc("5"); str_entered_AC[x++]='5'; break;
               case PB_6: lcd_putc("6"); str_entered_AC[x++]='6'; break;
               case PB_7: lcd_putc("7"); str_entered_AC[x++]='7'; break;
               case PB_8: lcd_putc("8"); str_entered_AC[x++]='8'; break;
               case PB_9: lcd_putc("9"); str_entered_AC[x++]='9'; break;

               case PB_LEFT:
               if(x>0)
               {
                  lcd_putc("\b");
                  x--;
               }
               break;

               case PB_RIGHT:
               if(x<=5)
               {
                  x++;
                  lcd_gotoxy(11+x,2);
               }
               break;

               case PB_OK_YES:
               ea_flag=LEAVE;
               break;

               case PB_BACK_NO:
               return 3;
               break;
           }

            // If in the 7th position, go back to 6th position
            // To prevent user from entering more than 6 digits
            if(x==6)
            {
               lcd_putc("\b");
               x--;
            }

            // Wait for push button press
            while( (pb=CheckIRinput()) == NOCMD && ea_flag==STAY);
         } while(ea_flag==STAY);
      }
   } while(ea_flag==STAY);

   return( atoi32(str_entered_AC) );
}


//--------------------------------------------------------------------------
//	Module:     enterStoneAndDisplayAC
//--------------------------------------------------------------------------
// Purpose:    Enter stone and display AC.
//
// Parameters:	None
//
// Returns:    CORRECT
//             WRONG
//             N_PRESSED
//
//--------------------------------------------------------------------------
#separate
U8 enterStoneAndDisplayAC (void)
{
   U8 retVal=0;

   random_stone = generate_stone();
   numStones--;
   totalNumStonesUsed++;
   splitU32Into4(&totalNumStonesUsed, &totalNumStonesUsed_hi,
   &totalNumStonesUsed_md, &totalNumStonesUsed_nd, &totalNumStonesUsed_lo);
   disable_interrupts(GLOBAL);
   write_eeprom(NUMSTONES_ADDR, numStones);
   write_eeprom(TOTALNUMSTONESUSED_HI_ADDR, totalNumStonesUsed_hi);
   write_eeprom(TOTALNUMSTONESUSED_MD_ADDR, totalNumStonesUsed_md);
   write_eeprom(TOTALNUMSTONESUSED_ND_ADDR, totalNumStonesUsed_nd);
   write_eeprom(TOTALNUMSTONESUSED_LO_ADDR, totalNumStonesUsed_lo);
   enable_interrupts(GLOBAL);
   lcd_putc("\f** NEW STONE **\n    CREATED");
   delay_ms(3000);

   actual_ac = generate_AC(random_stone);
   AC = enter_AC();
   if(AC==3)
   {
      retVal=N_PRESSED;
   }
   else if(AC==actual_AC)
   {
      lcd_putc("\f  A/C CORRECT\nMORE PINS ADDED");
      delay_ms(3000);
      numPINs += INITIALNUMPINS;
      if(numPINs >= MAXNUMPINS)
      {
         numPINs = MAXNUMPINS;
      }
      numStones = MAXNUMSTONES;
      disable_interrupts(GLOBAL);
      write_eeprom(NUMPINS_ADDR, numPINs);
      write_eeprom(NUMSTONES_ADDR, numStones);
      enable_interrupts(GLOBAL);
      retVal=CORRECT;
   }
   else // Wrong
   {
      lcd_send_byte(0,0x0C);     // NO blinking or underline cursor
      printf(lcd_putc,"\fA/C WRONG\nSTONES LEFT = %u", numStones);
      delay_ms(3000);
      retVal=WRONG;
   }

   return retVal;
}


//--------------------------------------------------------------------------
//	Module:     enterSeedAndDisplayPIN
//--------------------------------------------------------------------------
// Purpose:    Enter seed and display PIN.
//             Note: Do NOT allow user to use any other keys apart from OK to
//             exit.
//
// Parameters:	None
//
// Returns:    None
//
//--------------------------------------------------------------------------
#separate
void enterSeedAndDisplayPIN (void)
{
   if(numPINs>0) // Protection
   {
      seed = enter_seed();
      random_pin = generate_PIN(seed);
      numPINs--;  // Decrement no. of PIN's
      totalNumPINsUsed++;
      splitU32Into4(&totalNumPINsUsed, &totalNumPINsUsed_hi,
      &totalNumPINsUsed_md, &totalNumPINsUsed_nd, &totalNumPINsUsed_lo);
      disable_interrupts(GLOBAL);
      write_eeprom(TOTALNUMPINSUSED_HI_ADDR, totalNumPINsUsed_hi);
      write_eeprom(TOTALNUMPINSUSED_MD_ADDR, totalNumPINsUsed_md);
      write_eeprom(TOTALNUMPINSUSED_ND_ADDR, totalNumPINsUsed_nd);
      write_eeprom(TOTALNUMPINSUSED_LO_ADDR, totalNumPINsUsed_lo);
      write_eeprom(NUMPINS_ADDR, numPINs);
      enable_interrupts(GLOBAL);

      lcd_send_byte(0,0x0C);     // NO blinking or underline cursor
      do
      {
         printf(lcd_putc,"\fSEED = %06lu\nPIN  = %06lu  ",seed,random_pin);
         // Delay 5s and check any PB pressed
         delay_check_OK_PB(10);  // Delay 5s and check any PB pressed
         if(pb!=PB_OK_YES)
         {
            lcd_putc("\fOK = MENU");
            delay_check_OK_PB(4);  // Delay 2s and check any PB pressed
         }

      } while(pb!=PB_OK_YES); // Exit loop when a key is pressed
   }
}
