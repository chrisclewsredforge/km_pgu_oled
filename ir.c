/*****************************************************************************
      Red Forge Ltd
      Alders Drive
      Moons Moat East
      Redditch
      Worcs
      England
      B98 0RF

      Title:          Infrared (NEC format) receiver code
      Filename:       ir.c
      Issue:          V1.0
      Written by:     Chris Clews
      Modified by:
      Date:           Feb 2006
      Compiler:       CCS PCWH V3.242

      Red Forge Ltd owns the copyright in this document and associated
      documents and all rights are reserved.  This document and associated
      documents must not be used for any purpose other than that for which
      they are supplied and must not be copied in whole or in part, or
      disclosed to others without the prior written consent of Red Forge Ltd.
      Any copy of this document made by any method must also contain a copy
      of this legend.

               (c) Red Forge Ltd. 2004

--------------------------------------------------------------------------------

      CHANGE HISTORY
      Issue   Author      Date

      1.0	  C Clews     Feb 2006	   Timer2 used (prev T3)
      2.0     C Clews     March 2006   Multichar suppression .
                                       Accepts Celadon, Philips & TOTAL control remotes


*******************************************************************************/
/* VVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVV
Issue 2.0
Studied measured pulse lengths (low state). Adjusted "DATA_LO_MIN" - 500 to 480
Uses Timer1    ( set to 0 and free-running) to time between alternating edges
     new Iss2  (interrupt on rollover) to suppress multi-chars from i/red transmitter.
     Timer2 (interrupt on rollover) flags missing edge(s) = incomplete pulse train

   ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^ */
/*+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
To use this module:
Calling code must do the following PIC initialisation (or its equivalent):

   setup_timer_1(T1_INTERNAL|T1_DIV_BY_1);
   disable_interrupts(INT_TIMER2); // IRQs turned on when 1st edge detected.
   setup_timer_2(T2_DIV_BY_16,0,16);
   ext_int_edge(0, H_TO_L);
   enable_interrupts(INT_EXT);
   enable_interrupts(GLOBAL);


   SetUpRemoteKeypad();

   U8 Hex = CheckIRinput();// returns $FF if no keypress, or the hex generated
   by the keypress.
  +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/

#define  CELADON_ID           0x6E
#define  TOTAL_CONTROL_ID     0x18
#define  PHILIPS_ID           0x04


#define SYNC_LO_MIN           8000     // 8 ms     = 8ms/(4/4000000)
#define SYNC_LO_MAX           10000    // 10 ms
#define SYNC_PERIOD_MIN       12000    // 12 ms
#define SYNC_PERIOD_MAX       15000    // 15 ms
#define DATA_PERIOD_MIN       800      // 0.8 ms
#define DATA_PERIOD_MAX       2500     // 2.5 ms
#define DATA_LO_MIN           470 // Issue 2.0 reduced after tests... 8/3/2006
#define DATA_LO_MAX           750
#define LOGIC_01_THRESHOLD    1400     // 1.4 ms

// Codes which map unprintable keys on remote e.g. BACK/NO to code meaningful to caller
/*
#define  UP_KEY      'U'
#define  DOWN_KEY    'D'
#define  LEFT_KEY    'L'
#define  RIGHT_KEY   'R'
#define  BACK_NO_KEY 'B'
#define  OK_YES_KEY  'K'
#define  F1_KEY      'F'
#define  F2_KEY      '0'
*/

// The following 8 keys are assigned to arbitrary constants
#define  PB_UP       40
#define  PB_DOWN     41
#define  PB_LEFT     42
#define  PB_RIGHT    43
#define  PB_BACK_NO  44
#define  PB_OK_YES   45
#define  PB_F1       46
#define  PB_F2       47

#define  UP_KEY      PB_UP
#define  DOWN_KEY    PB_DOWN
#define  LEFT_KEY    PB_LEFT
#define  RIGHT_KEY   PB_RIGHT
#define  BACK_NO_KEY PB_BACK_NO
#define  OK_YES_KEY  PB_OK_YES
#define  F1_KEY      PB_F1
#define  F2_KEY      PB_F2

// The following 10 keys are assigned to output constants given in
// ConvertKeycode(Hex).
#define PB_0         48
#define PB_1         49
#define PB_2         50
#define PB_3         51
#define PB_4         52
#define PB_5         53
#define PB_6         54
#define PB_7         55
#define PB_8         56
#define PB_9         57

#define NOCMD        -1



#define SEPARATION_TIME  0 // For T1 between pulse trains - count up to overflow at 1 us

// If 32 msec passes without i/red edges(once 1st is detected), then the train is broken (pulse(s) missing)
// T2 in /16 mode with a period of 0 (=255 counts) at 4MHz.../4 and /16 gives an IRQ every 0.255 msec
// a U8 scaler is loaded with T2_SCALER and decremented each IRQ. To achieve 32 msec, the scaler is
// 32/0.255= 125.
#define T2_SCALER 125

// To suppress (detection of) multiple chars (for 1 keypress) use Timer 1 between pulse pkts.
// When the 1st edge is detected, stop T1 irqs.
// Timing the pulse low/periods uses T1 freerunning. Read and zeroed at each edge.
// When the last edge is detected, T1 is loaded with a countdown value "SEPARATION_TIME" and interrupts enabled.
// A discard flag is set TRUE - chars RX'ed in this state are ignored.
// When T1 interrupt occurs - after the chosen inter-char gap - this flag is set FALSE.
// This flag is inspected at the end of each received pulse train - rx_state==33 -
// and determines whether "got_frame" is set TRUE or FALSE.

/* For use with 18F6720
#use fast_io(b)               // Allows user to use output_c() to hi nibble
#use fast_io(c)               // without affecting the lo nibble of input pins.
#use fast_io(d)               // without affecting the lo nibble of input pins.

#byte port_a = 0xF80          // Give meaningful names for these ports
#byte port_b = 0xF81
#byte port_c = 0xF82
#byte port_d = 0xF83
#byte port_e = 0xF84
*/

unsigned int8 rx_state = 0xff;

// Custom and inverted custom codes
U8 custom_code=0, customi_code=0;

// Command number and inverted command number of key press
unsigned int8 cmd=0, cmdi=0;

// Checks to ensure custom and command number are valid
unsigned int8 check1=0, check2=0;

int1 got_frame = FALSE;    // (sync pulses + 32 bits of data) received

enum AwaitedEdge { WAITING_FOR_RISING_EDGE, WAITING_FOR_FALLING_EDGE};
enum AwaitedEdge ExpectedEdge = WAITING_FOR_FALLING_EDGE;


// >>>>>>>>>>>>>>>>> Function prototypes <<<<<<<<<<<<<<<<<<<<<<<<<<<<<
void EnableMissingPulseTimer();
void DisableMissingPulseTimer();
U8 ConvertKeycode(U8 IRcmd);


// Arrays for holding the time parameters for the elements of the pulse train.
U16 LowTimes[34];
U16 Periods[34];
U8 Bit[34];
U8 T2scaler;


U16 CharCount;
U16 BadSYNCperiod;
U16 BadSYNClow;
U16 BadBitLow;
U16 BadBitPeriod;
U8 junk;
U8 Temp;
U8 BrokenPulses;


bool DiscardMulti;


// Setup the conditions required prior to attempting reSYNC
void SetupForReSYNC()
{
  disable_interrupts(INT_EXT);
   rx_state = 0xff; // reset
   if  (ExpectedEdge == WAITING_FOR_RISING_EDGE) // currently set to interrupt on L_TO_H
   {
      ExpectedEdge = WAITING_FOR_FALLING_EDGE;
      ext_int_edge(0, H_TO_L);
   }
   enable_interrupts(INT_EXT);
}

void StartInterCharTimer( void)
{
   DiscardMulti = TRUE;// Flag the interchar gap as < that for multichars - to be discarded
   set_timer1(SEPARATION_TIME); // time gap NOT exceeded by multi-chars
   clear_interrupt(INT_TIMER1);
   enable_interrupts(INT_TIMER1);
}

void StopInterCharTimer( void)
{
   disable_interrupts(INT_TIMER1);
}

#int_TIMER1
void TIMER1_isr(void)
{
   DiscardMulti = FALSE;// interchar gap > that for multichars, don't discard
   StopInterCharTimer();
}




// Using T2 to detect missed/missing edges...(so OK for LL+)
// 4MHz->1MHz   /16 -> 62.5 nsec tickerX255X16->0.255 msec... so need scaler for 32 msec
// to get reqd interval.
#int_TIMER2
void TIMER2_isr(void)
{
   if  (--T2scaler==0)
   {// timeout before see expected edge ... lost pulse
      set_timer2(0);// 36 msec from rollover...
      T2scaler = T2_SCALER;// reprime scaler
      SetupForReSYNC();
      DisableMissingPulseTimer(); // stop IRQs from T2; it's done its job of forcing reSYNC
      BrokenPulses++;// debug counter...
// With remotes that send a train then several short
// trains, the fact that only the 1st train has all necessary pulses - there is rx_state=33 - means
// the T1 interchar gap timer is not restarted, discard flag remains TRUE, so pkts discarded...
// Use the broken train timeout (T2 + scaler) - on the shorter trains following the 1st -
// to start the interchar timer.
      StartInterCharTimer();// as would have occurred at end of train.
   }// if scaler down to 0
   else
   {
   }

}




#int_EXT
void ext_isr(void)
{
U16 TempTimerValue;

   restart_wdt();             // Reset watchdog to 2.3s +/- 50%
// Got (rising/falling) edge - still pulse activity, reset Timer3 and its counter
   set_timer2(0);// 36 msec from rollover...
   T2scaler = T2_SCALER;// reprime scaler
   if  (rx_state == 0xFF)// 1st edge new train.
   {
      StopInterCharTimer();// T1 needed to time pulse component lengths
   }

   if (ExpectedEdge == WAITING_FOR_FALLING_EDGE)
   { // This is a falling edge
      TempTimerValue = get_timer1();
      set_timer1(0);
      rx_state++;
      if  (rx_state != 0)// 0 means just got falling edge of (what might be) SYNC.
      {
         Periods[rx_state-1] = TempTimerValue;
      }
      else // getting SYNC pulse
      {
         got_frame = FALSE; // data for last pulse train no longer valid.
         EnableMissingPulseTimer();// keep lookout for broken train
      }
      ExpectedEdge = WAITING_FOR_RISING_EDGE;
      ext_int_edge(0, L_TO_H);
   }
   else
   { // This is a rising edge
      LowTimes[rx_state] = get_timer1();
      ExpectedEdge = WAITING_FOR_FALLING_EDGE;
      ext_int_edge(0, H_TO_L);

      if (rx_state == 33)// got reqd n/o falling edges - prev one was start EOT
                           //and now just got rising edge - end of EOT
      {
         SetupForReSYNC();
         // if this char has been RXed too "close" to the previous one, discard it.
         if  (DiscardMulti==TRUE)// got i/red train and too "close" to previous = discard
         {
            got_frame=FALSE;// discard the frame - multichar
            junk++;
         }
         else// separated by > margin, so not multi-char
         {
            got_frame=TRUE;
         }
         StartInterCharTimer();// end of char frame start multichar timer.
         DisableMissingPulseTimer();
      }
   }

}


// rx_state 0 is the sync pulse lo (about 9ms) and hi (about 4.2ms) times.
// rx_states 1 to 8 are custom code bits lo and hi times.
// rx_states 9 to 16 are inverted custom code bits lo and hi times.
// rx_states 17 to 24 are command bits lo and hi times.
// rx_states 25 to 32 are inverted command bits lo and hi times.
// rx_state 33 represents end of transmission (Lead out) lo (about 0.6ms)
// and hi times.


#separate


void SetUpRemoteKeypad()
{
   rx_state = 0xff;

// These are diagnostic/test variables - not needed.
   CharCount = 0;
   BadSYNCperiod = 0;
   BadSYNClow = 0;
   BadBitLow = 0;
   BadBitPeriod = 0;
   Temp= 0;
   DiscardMulti = TRUE;
   BrokenPulses = 0;
}

S8 CheckIRinput( void)
{
S8 RetVal;
U8 i, bit_index;

   RetVal = -1;//check OK keycode RX'ed on i/r receiver

   if (got_frame)// detected END pulse, check the data
   {
      restart_wdt();             // Reset watchdog to 2.3s +/- 50%
      custom_code = 0;// added 31/1/2006
      customi_code = 0;// added 31/1/2006
      got_frame = FALSE;
      // debugging
      if  (rx_state != 0xff)// next frame started...
      {
         bit_index=0;// for breakpoint
      }
//Know no break in train and reqd n/o edges seen. Analyse the timings for the pulse components.

// 1st check the SYNC
      if  (( LowTimes[0] >= SYNC_LO_MIN) && ( LowTimes[0] <= SYNC_LO_MAX))
      {
         if  ((Periods[0] >= SYNC_PERIOD_MIN) && (Periods[0] <= SYNC_PERIOD_MAX))// SYNC valid
         {
         // now the low time and period of each bit in train.
            for (i=1; i<33; i++)
            {
               if  ((LowTimes[i] >= DATA_LO_MIN) && ( LowTimes[i] <= DATA_LO_MAX))
               {
                  if  ((Periods[i] >= DATA_PERIOD_MIN) && (Periods[i] <= DATA_PERIOD_MAX))// valid logic bit
                  {
                  // durations are OK for 1 or 0 - need to identify bit from period (high time distinguishes)
                     bit_index = i-1;
                     if  (Periods[i] >= LOGIC_01_THRESHOLD) // logic 1
                     {
                     Bit[i] = 1;
                        if(bit_index>=0 && bit_index<=7)  bit_set(custom_code, bit_index);
                        else if(bit_index>=8 && bit_index<=15) bit_set(customi_code, bit_index-8);
                        else if(bit_index>=16 && bit_index<=23) bit_set(cmd, bit_index-16);
                        else if(bit_index>=24 && bit_index<=31) bit_set(cmdi, bit_index-24);
                     }
                     else // logic 0
                     {
                     Bit[i] = 0;
                        if(bit_index>=0 && bit_index<=7)  bit_clear(custom_code, bit_index);
                        else if(bit_index>=8 && bit_index<=15) bit_clear(customi_code, bit_index-8);
                        else if(bit_index>=16 && bit_index<=23) bit_clear(cmd, bit_index-16);
                        else if(bit_index>=24 && bit_index<=31) bit_clear(cmdi, bit_index-24);
                     }
                  }
                  else// Out of range period this bit
                  {
                     BadBitPeriod++;
                  }
               }
               else//Out of range Low time this bit
               {
                  BadBitLow++;
               }
            }
         }
         else// SYNC period wrong
         {
          BadSYNCperiod++;
         }
      }
      else// Bad SYNC - low portion
      {
         BadSYNClow++;
      }
      // Bits identified and folded into the requires bytes. Check data correct (1's compliment)
      check1 = custom_code ^ customi_code;
      check2 = cmd ^ cmdi;
      if  ( (custom_code==CELADON_ID) || (custom_code==TOTAL_CONTROL_ID) || (custom_code==PHILIPS_ID))// allow 2 types of remote
      {
         if   (check1==0xFF && check2==0xFF)
         {
//This now includes a call to convert the "cmd" to a device independent code...
             RetVal = ConvertKeycode(cmd);
             CharCount++;
         }
      }
      else// problem with decoding pulse train
      {
         Temp++;// for bpt

      }
// if ever need to check for unasserted bits, uncomment this loop
//       for  (i=0; i<34; i++)// Debug only.
//         Bit[i] = 0xFF;// so can see when neither 1 nor 0 written

   }
  return RetVal;
}

void EnableMissingPulseTimer()
{
   set_timer2(0);// 36 msec from rollover...
   T2scaler = T2_SCALER;// reprime scaler
   clear_interrupt(INT_TIMER2);//purge latched T2 IRQs
   enable_interrupts(INT_TIMER2);// keep lookout for broken train
}

void DisableMissingPulseTimer()
{
   disable_interrupts(INT_TIMER2);
}




U8 ConvertKeycode(U8 IRcmd)
{
U8 RetVal = ' ';

   restart_wdt();             // Reset watchdog to 2.3s +/- 50%
   if  (custom_code==CELADON_ID)
   {
      if  ( (IRcmd >= 0x0F) && (IRcmd<=0x17))// '1' - '9'
      {
         RetVal = (IRcmd - 0x0E)+'0';
      }
      else if  (IRcmd == 0x19)
      {
         RetVal = '0';
      }
      else// others are "positioning"/"response" keys deal with in calling code
      {
         switch (IRcmd)
         {
            case  7:
               RetVal = UP_KEY;// UP
            break;

            case 0x0D:
               RetVal = DOWN_KEY;//DOWN
            break;

            case  0x09:
               RetVal = LEFT_KEY;// LEFT
            break;

            case 0x0B:
               RetVal = RIGHT_KEY; // RIGHT
            break;

            case 0x0A:
               RetVal = OK_YES_KEY; // oK/yes
            break;

            case 0x02:
               RetVal = BACK_NO_KEY; // BACK
            break;

            case  00:
               RetVal = F1_KEY; // F1
            break;

            case  03:
               RetVal = F2_KEY;// F2
            break;

            default:
            break;
         }


      }
   }  // above block is for Celadon

   else if  (custom_code==TOTAL_CONTROL_ID)
   {
      if  ( (IRcmd >= 0x10) && (IRcmd<=0x18))// '1' - '9'
      {
         RetVal = (IRcmd - 0x0F)+'0';
      }
      else if  (IRcmd == 0x1A)
      {
         RetVal = '0';
      }
      else// others are "positioning"/"response" keys deal with in calling code
      {
         switch (IRcmd)
         {
            case  0:
               RetVal = UP_KEY;// UP
            break;

            case 0x01:
               RetVal = DOWN_KEY;//DOWN
            break;

            case  0x03:
               RetVal = LEFT_KEY;// LEFT
            break;

            case 0x02:
               RetVal = RIGHT_KEY; // RIGHT
            break;

            case 0x09:
               RetVal = OK_YES_KEY; // oK/yes
            break;

            case 0x0A:
               RetVal = BACK_NO_KEY; // BACK
            break;

            case  0x53:
               RetVal = F1_KEY; // F1
            break;

            case  0x56:
               RetVal = F2_KEY;// F2
            break;

            default:
            break;
         }
      }
   }
   else if  (custom_code==PHILIPS_ID)
   {
      if  ( (IRcmd >= 0x10) && (IRcmd<=0x19))// '0' - '9'
      {
         RetVal = (IRcmd - 0x10)+'0';
      }
      else// others are "positioning"/"response" keys deal with in calling code
      {
         switch (IRcmd)
         {
            case  0:
               RetVal = UP_KEY;// UP
            break;

            case 0x01:
               RetVal = DOWN_KEY;//DOWN
            break;

            case  0x03:
               RetVal = LEFT_KEY;// LEFT
            break;

            case 0x02:
               RetVal = RIGHT_KEY; // RIGHT
            break;

            case 0x4C:
               RetVal = OK_YES_KEY; // oK/yes
            break;

            case 0x08:
               RetVal = BACK_NO_KEY; // BACK
            break;

            case  0x0A:
               RetVal = F1_KEY; // F1
            break;

            case  0x09:
               RetVal = F2_KEY;// F2
            break;

            default:
            break;
         }
      }
   }

   return RetVal;
}

