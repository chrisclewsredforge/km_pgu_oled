/*****************************************************************************
      Red Forge Ltd
      9, Palmers Road
      Moons Moat East
      Redditch
      Worcs
      England
      B98 0RF

      Title:          PGU Plus routines
      Filename:       pguplus_routines.c
      Issue:          V2.0
      Written by:     Tommy Liu
      Modified by:    Tommy Liu
      Date:           11 Mar 2010
      Compiler:       CCS PCWH V3.249

      Red Forge Ltd owns the copyright in this document and associated
      documents and all rights are reserved.  This document and associated
      documents must not be used for any purpose other than that for which
      they are supplied and must not be copied in whole or in part, or
      disclosed to others without the prior written consent of Red Forge Ltd.
      Any copy of this document made by any method must also contain a copy
      of this legend.

               (c) Red Forge Ltd. 2010

--------------------------------------------------------------------------------

      CHANGE HISTORY
      Issue   Author      Date

      1.0     T Liu	     13 Oct 2006
      First Issue.

      2.0     T Liu	     11 Mar 2010
      - Added control over body shop regarding no. of stones and no. of PIN's.
      - See pguplus_misc.c.
      - See header.h

*******************************************************************************/

// DEFINES
#define k1     321   // key values
#define k2     3
#define k3     123
#define k4     15
#define delta  456

#define delta2  789

// Constant allows random numbers to be generated between 0 and 999.
#define RAND_MAX_RF  999



//---------------------------------------------------------------------------
// The random number implementation
// Note: The following 2 functions were copied from STDLIB.H to order
//       to reduce code size.
//
//       The function names, constant and variable names have been appended
//       with "_RF" ie. Red Forge.
//
//       RAND_MAX_RF changed from 32767 to 999.
//
//---------------------------------------------------------------------------
U32 _Randseed_RF = 1;

U16 rand_RF(void)
{
   _Randseed_RF = _Randseed_RF * 1103515245 + 12345;
   return ((U16)(_Randseed_RF >> 16) % RAND_MAX_RF);
}

void srand_RF(U32 seed_RF)
{
   _Randseed_RF = seed_RF;
}


//--------------------------------------------------------------------------
//	Module:     FUNCTION A - generate_seed
//--------------------------------------------------------------------------
// Purpose:    Generate a random 6-digit seed
//
// Parameters:	None
//
// Returns:    6-digit seed
//
//--------------------------------------------------------------------------
#separate
U32 generate_seed (void)
{
   U16 s1, s2;

   srand_RF(get_timer3()); // Use a variable "seed" for rand_RF().
   s1=rand_RF();           // Obtain random 3-digit number.
   srand_RF(get_timer3()); // Use a variable "seed" for rand_RF().
   s2=rand_RF();           // Obtain random 3-digit number.

   // ie. s1 and S2 are between 0 and 999.
   return((U32)s1*1000 + s2);  // Combine s1 and s2 to form a seed
}


//--------------------------------------------------------------------------
//	Module:     FUNCTION B - generate_PIN
//--------------------------------------------------------------------------
// Purpose:    Generate a 6-digit PIN from a 6-digit seed
//
// Parameters:	6-digit seed from user
//
// Returns:    6-digit PIN
//
//--------------------------------------------------------------------------
#separate
U32 generate_PIN (U32 seed_value)
{
   U32 y, z;          // Upper and lower seed values
   U16 sum=0;         // Sum of iterations
   U8 n=4;            // Number of iterations

   y=seed_value/1000;
   z=seed_value%1000;

   while(n>0)
   {
      sum += delta;
      y += z<<1;
      y += k1^z;
      y += sum^(z>>2);
      y += k2;

      z += y<<1;
      z += k3^y;
      z += sum^(y>>2);
      z += k4;

      n--;
   }

   y %= 1000;           // Calculate last 3 least sig digits
   z %= 1000;           // Calculate last 3 least sig digits

   return( ((U32)y*1000) + z ); // Combine y and z to give 6-digit PIN
}


//--------------------------------------------------------------------------
//	Module:     FUNCTION C - generate_stone
//--------------------------------------------------------------------------
// Purpose:    Generate a random 6-digit stone
//
// Parameters:	None
//
// Returns:    6-digit stone
//
//--------------------------------------------------------------------------
#separate
U32 generate_stone (void)
{
   U16 s1, s2;

   srand_RF(get_timer3()); // Use a variable "seed" for rand_RF().
   s1=rand_RF();           // Obtain random 3-digit number.
   srand_RF(get_timer3()); // Use a variable "seed" for rand_RF().
   s2=rand_RF();           // Obtain random 3-digit number.

   // ie. s1 and S2 are between 0 and 999.
   return((U32)s1*1000 + s2);  // Combine s1 and s2 to form a stone
}


//--------------------------------------------------------------------------
//	Module:     FUNCTION D - generate_AC
//--------------------------------------------------------------------------
// Purpose:    Generate a 6-digit activation code from a 6-digit stone
//
// Parameters:	6-digit stone from user
//
// Returns:    6-digit activation code
//
// MOTE:       2 differences to FUNCTION B; n and delta2.

//--------------------------------------------------------------------------
#separate
U32 generate_AC (U32 stone_value)
{
   U32 y, z;          // Upper and lower seed values
   U16 sum=0;         // Sum of iterations
   U8 n=5;            // Number of iterations *** DIFF1 ***

   y=stone_value/1000;
   z=stone_value%1000;

   while(n>0)
   {
      sum += delta2;    // *** DIFF2 ***
      y += z<<1;
      y += k1^z;
      y += sum^(z>>2);
      y += k2;

      z += y<<1;
      z += k3^y;
      z += sum^(y>>2);
      z += k4;

      n--;
   }

   y %= 1000;           // Calculate last 3 least sig digits
   z %= 1000;           // Calculate last 3 least sig digits

   return( ((U32)y*1000) + z ); // Combine y and z to give 6-digit AC
}


//--------------------------------------------------------------------------
//	Module:     enter_seed
//--------------------------------------------------------------------------
// Purpose:    Enter 6-digit seed and convert its string into a 6 digit int32
//             value.
//
// Parameters:	None
//
// Returns:    6 digit seed in int32
//
//--------------------------------------------------------------------------
#separate
U32 enter_seed (void)
{
   U8 x=0;
   bool es_flag=STAY;

   // Note: x=0 is the 1st digit of seed
   //       x=5 is the 6th digit of seed

   do
   {
      es_flag=STAY;             // Required for seed re-entry

      lcd_putc("\fENTER SEED\n______ THEN OK");
      lcd_gotoxy(1,2);                      // Position cursor
      lcd_send_byte(0,0x0D);                // Blinking cursor
      while( (pb=CheckIRinput()) == NOCMD); // Wait for push button press
      if( (pb>=PB_0 && pb<=PB_9) )
      {
         do
         {
            switch(pb)
            {
               case PB_0: lcd_putc("0"); str_entered_seed[x++]='0'; break;
               case PB_1: lcd_putc("1"); str_entered_seed[x++]='1'; break;
               case PB_2: lcd_putc("2"); str_entered_seed[x++]='2'; break;
               case PB_3: lcd_putc("3"); str_entered_seed[x++]='3'; break;
               case PB_4: lcd_putc("4"); str_entered_seed[x++]='4'; break;
               case PB_5: lcd_putc("5"); str_entered_seed[x++]='5'; break;
               case PB_6: lcd_putc("6"); str_entered_seed[x++]='6'; break;
               case PB_7: lcd_putc("7"); str_entered_seed[x++]='7'; break;
               case PB_8: lcd_putc("8"); str_entered_seed[x++]='8'; break;
               case PB_9: lcd_putc("9"); str_entered_seed[x++]='9'; break;

               case PB_LEFT:
               if(x>0)
               {
                  lcd_putc("\b");
                  x--;
               }
               break;

               case PB_RIGHT:
               if(x<=5)
               {
                  x++;
                  lcd_gotoxy(x+1,2);
               }
               break;

               case PB_OK_YES:
               es_flag=LEAVE;
               break;

            }

            // If in the 7th position, go back to 6th position
            // To prevent user from entering more than 6 digits
            if(x==6)
            {
               lcd_putc("\b");
               x--;
            }

            // Wait for push button press
            while( (pb=CheckIRinput()) == NOCMD && es_flag==STAY);
         } while(es_flag==STAY);
      }
   } while(es_flag==STAY);

   return( atoi32(str_entered_seed) );
}


#separate
void pgu_function (void)
{
   U8 result=0;
   bool flag_inner=STAY;
   bool flag_outer=STAY;

   if(!PB_OK_YES_flag)
   {
      lcd_putc("\f PGU PLUS V2.0");
      delay_check_OK_PB(3);      // Delay 1.5s
   }

   // If OK key was pressed
   if(PB_OK_YES_flag)
   {
      displayData();
   }

   while(TRUE)
   {
      do
      {
         flag_outer=STAY;
         lcd_send_byte(0,0x0C);     // NO blinking or underline cursor
         checkStones();
         do
         {
            flag_inner=STAY;
            lcd_putc("\f1 = ENTER SEED\n2 = ENTER A/C");
            do
            {
               while( (pb=CheckIRinput()) == NOCMD);  // Wait for push button
            } while(pb!=PB_1 && pb!=PB_2);
            // Wait until 1 or 2 is pressed
            if(pb==PB_2)
            {
               printf(lcd_putc,"\fNo. OF STONES\nLEFT = %u", numStones);
               delay_ms(3000);

               result = checkPINsEnterStone();
               if(result == TOO_MANY_PINS)
               {
                  flag_inner=STAY;
               }
               else // Any other result
               {
                  flag_inner=LEAVE;
               }
            }
            else // pb==PB_1
            {
               flag_inner=LEAVE;
               flag_outer=LEAVE;
            }
         } while(flag_inner==STAY);
      } while(flag_outer==STAY);

      // 1 pressed...
      printf(lcd_putc,"\fNo. OF PINS\nLEFT = %u", numPINs);
      delay_ms(3000);

      if(numPINs==0)
      {
         do
         {
            lcd_putc("\f  NO MORE PINS\n CALL RED FORGE");
            // Delay 5s and check any PB pressed
            delay_check_OK_PB(4);     // Delay 2s and check PB pressed
            if(!PB_OK_YES_flag)
            {
               lcd_putc("\fOK = MENU\nTHEN ENTER A/C");
               delay_check_OK_PB(4);  // Delay 2s and check PB pressed
            }
         } while(pb!=PB_OK_YES);   // Exit loop when a key is pressed
      }
      // There are still PINs left...
      else // numPINs>0
      {
         enterSeedAndDisplayPIN();
         // OK just pressed
      }
   }
}
