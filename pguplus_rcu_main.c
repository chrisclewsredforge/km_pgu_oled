/*****************************************************************************
      Red Forge Ltd
      Palmers Road
      Moons Moat East
      Redditch
      Worcs
      England
      B98 0RF

      Title:          Main source file for PGUPLUS/RCU (12V Version)
      Filename:       pguplus_rcu.c
      Issue:          V2.0
      Written by:     Tommy Liu
      Modified by:    Tommy Liu
      Date:           11 Mar 2010
      Compiler:       CCS PCWH V3.249


      Red Forge Ltd owns the copyright in this document and associated
      documents and all rights are reserved.  This document and associated
      documents must not be used for any purpose other than that for which
      they are supplied and must not be copied in whole or in part, or
      disclosed to others without the prior written consent of Red Forge Ltd.
      Any copy of this document made by any method must also contain a copy
      of this legend.

               (c) Red Forge Ltd. 2010

--------------------------------------------------------------------------------
      SUMMARY
      PGUPLUS:
      a. This converts a 6-digit seed into a 6-digit random PIN.

      The seed is obtained from the LL+ or Axalert2 display and entered
      by the user using the remote control keys.

      The output is a 6-digit PIN.

      b. This converts a 6-digit random stone (from the PGUPLUS) into a 6-digit
      A/C (activation code).

      The stone is obtained from the PGUPLUS and the body shop gives the stone
      to Red Forge who then gives the correct A/C to the body shop to enter into
      the PGUPLUS using the remote control keys. This will give the body shop
      more PIN's.


      RCU:
      Allows the user to display the sensor voltage as a digital value. Also,
      when the voltage is between 3.93V and 4.05V, the sounder beeps.

      A single line marker is displayed on the analogue display when the
      voltage is between 3.93V and 4.05V. The marker has 3 lines when voltage
      is =<3.81V or >=4.17V.

--------------------------------------------------------------------------------

      CHANGE HISTORY
      Issue   Author      Date

      1.0	  T Liu	     16 Oct 2008
      First Issue.

      2.0	  T Liu	     11 Mar 2010
      Changes to PGUPLUS only.
      - See pguplus_routines.c.

*******************************************************************************/
#include "header.h"           // Header file contains contants and include file

#use fast_io(a)               // Allows user to use "set_tris_a(X)"
#use fast_io(b)               // Allows user to use output_c() to hi nibble
#use fast_io(c)               // without affecting the lo nibble of input pins.

#byte port_a = 0xF80          // Give meaningful names for these ports
#byte port_b = 0xF81
#byte port_c = 0xF82

// GLOBAL VARIABLES are here as usual

// VARIABLES FOR RCU
// Definitions for special LCD chars (not in LCD ROM library)
char const fullline[8]     = {0x15,0x15,0x15,0x15,0x15,0x15,0x15,0x15};
char const left2line[8]    = {0x10,0x10,0x10,0x10,0x10,0x10,0x10,0x10};
char const left1line[8]    = {0x08,0x08,0x08,0x08,0x08,0x08,0x08,0x08};
char const centreline[8]   = {0x04,0x04,0x04,0x04,0x04,0x04,0x04,0x04};
char const right1line[8]   = {0x02,0x02,0x02,0x02,0x02,0x02,0x02,0x02};
char const right2line[8]   = {0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01};

U16 vin;                       // Sensor input digital voltage

U8 col=1;                     // LCD column position

bool sounding_flag = DISABLED;  // Flag to indicate whether sounder is ON/OFF


// VARIABLES FOR PGU
U32 AC=0;
U32 seed=0;                 // 6-digit random seed for generating random PIN.
U32 random_pin=0;           // Output 6-digit random PIN.
U32 random_stone=0;         // Output 6-digit random stone.
U32 actual_ac=0;
U32 totalNumStonesUsed=0;
U32 totalNumPINsUsed=0;


char str_entered_seed[7] ="000000";
char str_entered_ac[7]   ="000000";

U8 numPINs = 0;
U8 numStones = 0;
U8 timer3_overflow_startup=0;

U8 totalNumStonesUsed_hi=0;
U8 totalNumStonesUsed_md=0;
U8 totalNumStonesUsed_nd=0;
U8 totalNumStonesUsed_lo=0;

U8 totalNumPINsUsed_hi=0;
U8 totalNumPINsUsed_md=0;
U8 totalNumPINsUsed_nd=0;
U8 totalNumPINsUsed_lo=0;

S8 pb;               // Used for storing push button command

bool PB_1_flag=FALSE;
bool PB_2_flag=FALSE;
bool PB_OK_YES_flag=FALSE;
bool PB_F2_flag=FALSE;
bool dummy5=FALSE;
bool dummy6=FALSE;
bool dummy7=FALSE;
bool dummy8=FALSE;


#include <stdlib.h>        // For atoi32()
#include "lcd1.c"
#include "mcu.c"
#include "ir.c"
#include "delays.c"
#include "pguplus_misc.c"
#include "pguplus_routines.c"
#include "rcu_routines.c"



void main(void)
{
   setup_ports();                   // Configure MCU ports
   output_low(SOUNDER);             // Disable sounder on start up
   configure_ADC_ports();           // Configure ADC ports

   // Write defaults if just programmed for new unit
   write_defaults();

   read_eeprom_data();

   // Timer3
   // Time base for each interrupt = 500ms
   // Tick time = 8us
   // Used for the following:
   setup_timer_3(T3_INTERNAL | T3_DIV_BY_8);
   set_timer3(T500msValue);  // Reload Timer3

   setup_comparator(NC_NC_NC_NC);
   setup_vref(FALSE);

   // Timer1 is IR to time pulse widths (for PGU)
   setup_timer_1(T1_INTERNAL | T1_DIV_BY_1);

   // Timer2 used for 32ms timing in IR (for PGU)
   // prescaler = 16
   // postscaler - perform 16 overflow counts before interrupt
   disable_interrupts(INT_TIMER2); // IRQs turned on when 1st edge detected.
   setup_timer_2(T2_DIV_BY_16,0,16);

   // For IR (for PGU)
   ext_int_edge(0, H_TO_L);
   enable_interrupts(INT_EXT);

   // For IR (for PGU)
   SetUpRemoteKeypad();

   enable_interrupts(GLOBAL);

   lcd_init();                // Initialise LCD
   delay_ms(6);               // Allow LCD to settle

//   create_CGRAM_chars();      // Create custom LCD chars

   clear_interrupt(INT_TIMER3);
   enable_interrupts(INT_TIMER3);

 //  check_bat();               // Check battery voltage on startup

   // If OK key not pressed
   if(!PB_OK_YES_flag)
   {
      lcd_putc("\f RED FORGE LTD\n redforge.co.uk");
      delay_check_OK_PB(3);      // Delay 1.5s
   }

   if( input(SELECT_SW)==PGU )
   {
      pgu_function();
   }
   else // ( input(SELECT_SW)==RCU )
   {
      disable_interrupts(INT_TIMER1);
      disable_interrupts(INT_TIMER2);
      disable_interrupts(INT_TIMER3);
      disable_interrupts(INT_EXT);
      rcu_function();
   }
}


