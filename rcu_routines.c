/*****************************************************************************
      Red Forge Ltd
      9, Palmers Road
      Moons Moat East
      Redditch
      Worcs
      England
      B98 0RF

      Title:          RCU routines
      Filename:       rcu_routines.c
      Issue:          V1.5
      Written by:     Tommy Liu
      Modified by:
      Date:           13 Oct 2006
      Compiler:       CCS PCWH V3.249

      Red Forge Ltd owns the copyright in this document and associated
      documents and all rights are reserved.  This document and associated
      documents must not be used for any purpose other than that for which
      they are supplied and must not be copied in whole or in part, or
      disclosed to others without the prior written consent of Red Forge Ltd.
      Any copy of this document made by any method must also contain a copy
      of this legend.

               (c) Red Forge Ltd. 2006

--------------------------------------------------------------------------------

      CHANGE HISTORY
      Issue   Author      Date

      1.5     T Liu	     13 Oct 2006
      First Issue.

*******************************************************************************/
#separate
void rcu_function (void)
{
   // Timer0 used for pulsing (50ms ON/50ms OFF) the sounder (for RCU).
   setup_timer_0(RTCC_INTERNAL|RTCC_DIV_256);

   lcd_putc("\f RCU V1.5(12V)");
   delay_ms(1000);

   lcd_putc("\f CONNECT SENSOR\n  TO THIS UNIT");
   delay_ms(1000);

   lcd_putc("\fAND ROTATE SHAFT\n TO VARY WEIGHT");
   delay_ms(1000);

   display_main_top_line();   // Display top line normal display

   set_adc_channel(0);        // Select channel to connect to sensor
   delay_ms(1);

   while(TRUE)
   {
      display_main_top_line(); // Refresh top line normal display

      clear_bottom_line();    // Clear all 16 LCD bottom segments

      display_bottom_line();  // Display bargraph output for sensor voltage

      delay_ms(330);          // Wait 330ms (ie. about 3 readings per second)

   }
}


//--------------------------------------------------------------------------
//	Module:  Timer0 ISR
//--------------------------------------------------------------------------
// Purpose: Use timer0 to give the following pulse train on internal
//          SOUNDER pin:
//
//
//  ____|     |_________________________|     |_________
//       100ms          500ms            100ms
//
// Parameters:	None
//
// Returns:    None
//
//--------------------------------------------------------------------------
#int_TIMER0
void SOUNDER_ISR(void)
{
   static int8 count_lo=0, count_hi=0;

   // HI for 100ms
   if(input(SOUNDER)==HIGH)
   {
      count_hi++;
      if(count_hi>=2)
      {
         count_hi=0;
         output_low(SOUNDER);      // Low
         set_timer0(T50msValue);   // Reload timer with 50ms
      }
   }
   // LO for 500ms
   else // input(SOUNDER)==LOW
   {
      count_lo++;
      if(count_lo>=10)
      {
         count_lo=0;
         output_high(SOUNDER);     // High
         set_timer0(T50msValue);   // Reload timer with 50ms
      }
   }
}


//--------------------------------------------------------------------------
//	Module:  control_sounder
//--------------------------------------------------------------------------
// Purpose: Enable Timer0 interrupt and set SOUNDER pin high
//
// Parameters:	None
//
// Returns:    None
//
//--------------------------------------------------------------------------
#separate
void enable_sounder (void)
{
   enable_interrupts(INT_TIMER0);
   output_high(SOUNDER);            // Start with SOUNDER high
   sounding_flag = ENABLED;
}


//--------------------------------------------------------------------------
//	Module:  disable_sounder
//--------------------------------------------------------------------------
// Purpose: Disable Timer0 interrupt and set SOUNDER pin low
//
// Parameters:	None
//
// Returns:    None
//
//--------------------------------------------------------------------------
#separate
void disable_sounder (void)
{
   disable_interrupts(INT_TIMER0);
   output_low(SOUNDER);             // Disable sounder
   sounding_flag = DISABLED;
}


//--------------------------------------------------------------------------
//	Module:  find_column
//--------------------------------------------------------------------------
// Purpose: Determine column on LCD bottom line wrt to input digital voltage.
//          There are 16 segments and each segment consists of 5 columns.
//          Segment 1 consists of columns 11 to 15, segment 2 consists of
//          columns 21 to 25 and so on. Column 11 is at the far LHS at segment
//          1, and column 165 is at the far RHS at segment 16.  3.5V is at
//          column 85.
//
// Parameters:	None
//
// Returns:    None
//
//--------------------------------------------------------------------------
#separate
void find_column(void)
{
   if(vin<=653) col=115;                  // <3.19V
   else if(vin>653 && vin<=776) col=115;  // 3.19V to 3.79V

   else if(vin>776 && vin<=780) col=105;  // 3.79V to 3.81V )
   else if(vin>780 && vin<=784) col=104;  // 3.81V to 3.83V )
   else if(vin>784 && vin<=788) col=103;  // 3.83V to 3.85V )
   else if(vin>788 && vin<=792) col=102;  // 3.85V to 3.87V )
   else if(vin>792 && vin<=796) col=101;  // 3.87V to 3.89V )
   else if(vin>796 && vin<=800) col=95;   // 3.89V to 3.91V )
   else if(vin>800 && vin<=805) col=94;   // 3.91V to 3.93V )
   else if(vin>805 && vin<=809) col=93;   // 3.93V to 3.95V )
   else if(vin>809 && vin<=813) col=92;   // 3.95V to 3.97V )
   else if(vin>813 && vin<=817) col=91;   // 3.97V to 3.99V )
   else if(vin>817 && vin<=821) col=85;   // 3.99V to 4.01V ) Display voltage
   else if(vin>821 && vin<=825) col=84;   // 4.01V to 4.03V )
   else if(vin>825 && vin<=829) col=83;   // 4.03V to 4.05V )
   else if(vin>829 && vin<=833) col=82;   // 4.05V to 4.07V )
   else if(vin>833 && vin<=837) col=81;   // 4.07V to 4.09V )
   else if(vin>837 && vin<=841) col=75;   // 4.09V to 4.11V )
   else if(vin>841 && vin<=845) col=74;   // 4.11V to 4.13V )
   else if(vin>845 && vin<=850) col=73;   // 4.13V to 4.15V )
   else if(vin>850 && vin<=854) col=72;   // 4.15V to 4.17V )
   else if(vin>854 && vin<=858) col=71;   // 4.17V to 4.19V )

   else if(vin>858 && vin<=878) col=65;   // 4.19V to 4.29V
   else if(vin>878 && vin<=899) col=55;   // 4.29V to 4.39V
   else if(vin>899 && vin<=919) col=45;   // 4.39V to 4.49V
   else if(vin>919 && vin<=940) col=35;   // 4.49V to 4.59V
   else if(vin>940 && vin<=960) col=25;   // 4.59V to 4.69V
   else if(vin>960 && vin<=981) col=15;   // 4.69V to 4.79V
   else if(vin>981) col=15;               // >4.79V
}

/* Note: For V1.4 or older
//--------------------------------------------------------------------------
//	Module:  find_column
//--------------------------------------------------------------------------
// Purpose: Determine column on LCD bottom line wrt to input digital voltage.
//          There are 16 segments and each segment consists of 5 columns.
//          Segment 1 consists of columns 11 to 15, segment 2 consists of
//          columns 21 to 25 and so on. Column 11 is at the far LHS at segment
//          1, and column 165 is at the far RHS at segment 16.  3.5V is at
//          column 85.
//
// Parameters:	None
//
// Returns:    None
//
//--------------------------------------------------------------------------
#separate
void find_column(void)
{
   if(vin<=550) col=115;                  // <2.69V
   else if(vin>550 && vin<=673) col=115;  // 2.69V to 3.29V

   else if(vin>673 && vin<=677) col=105;  // 3.29V to 3.31V )
   else if(vin>677 && vin<=681) col=104;  // 3.31V to 3.33V )
   else if(vin>681 && vin<=685) col=103;  // 3.33V to 3.35V )
   else if(vin>685 && vin<=689) col=102;  // 3.35V to 3.37V )
   else if(vin>689 && vin<=693) col=101;  // 3.37V to 3.39V )
   else if(vin>693 && vin<=697) col=95;   // 3.39V to 3.41V )
   else if(vin>697 && vin<=701) col=94;   // 3.41V to 3.43V )
   else if(vin>701 && vin<=705) col=93;   // 3.43V to 3.45V )
   else if(vin>705 && vin<=709) col=92;   // 3.45V to 3.47V )
   else if(vin>709 && vin<=714) col=91;   // 3.47V to 3.49V )
   else if(vin>714 && vin<=718) col=85;   // 3.49V to 3.51V ) Display voltage
   else if(vin>718 && vin<=722) col=84;   // 3.51V to 3.53V )
   else if(vin>722 && vin<=726) col=83;   // 3.53V to 3.55V )
   else if(vin>726 && vin<=730) col=82;   // 3.55V to 3.57V )
   else if(vin>730 && vin<=734) col=81;   // 3.57V to 3.59V )
   else if(vin>734 && vin<=738) col=75;   // 3.59V to 3.61V )
   else if(vin>738 && vin<=742) col=74;   // 3.61V to 3.63V )
   else if(vin>742 && vin<=746) col=73;   // 3.63V to 3.65V )
   else if(vin>746 && vin<=750) col=72;   // 3.65V to 3.67V )
   else if(vin>750 && vin<=754) col=71;   // 3.67V to 3.69V )

   else if(vin>754 && vin<=775) col=65;   // 3.69V to 3.79V
   else if(vin>775 && vin<=795) col=55;   // 3.79V to 3.89V
   else if(vin>795 && vin<=816) col=45;   // 3.89V to 3.99V
   else if(vin>816 && vin<=836) col=35;   // 3.99V to 4.09V
   else if(vin>836 && vin<=857) col=25;   // 4.09V to 4.19V
   else if(vin>857 && vin<=877) col=15;   // 4.19V to 4.29V
   else if(vin>877) col=15;               // >4.29V
}
*/

//--------------------------------------------------------------------------
//	Module:  display_voltage
//--------------------------------------------------------------------------
// Purpose: Display voltage on LCD bottom line.
//
// Parameters:	None
//
// Returns:    None
//
//--------------------------------------------------------------------------
#separate
void display_voltage(void)
{
   lcd_gotoxy(12,2);                 // Go to 2nd line, 1st position
   printf(lcd_putc,"%3.2fV", vin*DIV_5VBY1024);
}

//--------------------------------------------------------------------------
//	Module:  display_marker
//--------------------------------------------------------------------------
// Purpose: Display the single line or 3-line marker on the corresponding
//          column.
//
// Parameters:	None
//
// Returns:    None
//
//--------------------------------------------------------------------------
#separate
void display_marker(void)
{

   lcd_gotoxy(col/10,2);            // Go to LCD position

   if((col>=10 && col<=65) || (col>=110 && col<=115))
   {
      lcd_putc(FLINE);              // Display 3 line marker
      disable_sounder();            // Disable sounder
   }
   else if(col==71)
   {
      lcd_putc(L2LINE);             // Display #1 left line marker
      disable_sounder();            // Disable sounder
   }
   else if(col==72)
   {
      lcd_putc(L2LINE);             // Display #2 left line marker
      disable_sounder();            // Disable sounder
   }
   else if(col==73)
   {
      lcd_putc(CLINE);             // Display #2 cenre line marker
      disable_sounder();            // Disable sounder
   }
   else if(col==74)
   {
      lcd_putc(R1LINE);             // Display #1 right line marker
      disable_sounder();            // Disable sounder
   }
   else if(col==75)
   {
      lcd_putc(R2LINE);             // Display #2 right line marker
      disable_sounder();            // Disable sounder
   }
   else if(col==81)
   {
      lcd_putc(L2LINE);             // Display #2 left line marker
      disable_sounder();            // Disable sounder
   }
   else if(col==82)
   {
      lcd_putc(L1LINE);             // Display #1 left line marker
   }
   else if(col==83)
   {
      lcd_putc(CLINE);              // Display centre line marker
   }
   else if(col==84)
   {
      lcd_putc(R1LINE);             // Display #1 right line marker
   }
   else if(col==85)
   {
      lcd_putc(R2LINE);             // Display #2 right line marker
                                    // Centre mark for 4.00V
   }
   else if(col==91)
   {
      lcd_putc(L2LINE);             // Display #2 left line marker
   }
   else if(col==92)
   {
      lcd_putc(L1LINE);             // Display #1 left line marker
   }
   else if(col==93)
   {
      lcd_putc(CLINE);              // Display centre line marker
   }
   else if(col==94)
   {
      lcd_putc(R1LINE);             // Display #1 right line marker
      disable_sounder();            // Disable sounder
   }
   else if(col==95)
   {
      lcd_putc(R2LINE);             // Display #2 right line marker
      disable_sounder();            // Disable sounder
   }
   else if(col==101)
   {
      lcd_putc(L2LINE);             // Display #2 left line marker
      disable_sounder();            // Disable sounder
   }
   else if(col==102)
   {
      lcd_putc(L2LINE);             // Display #2 left line marker
      disable_sounder();            // Disable sounder
   }
   else if(col==103)
   {
      lcd_putc(CLINE);             // Display #2 cenre line marker
      disable_sounder();            // Disable sounder
   }
   else if(col==104)
   {
      lcd_putc(R1LINE);             // Display #1 right line marker
      disable_sounder();            // Disable sounder
   }
   else if(col==105)
   {
      lcd_putc(R2LINE);             // Display #2 right line marker
      disable_sounder();            // Disable sounder
   }

   if(col>=82 && col<=93)
   {
      if(sounding_flag==DISABLED)
         enable_sounder();          // Enable sounder
   }

   display_voltage();            // Display voltage
}

//--------------------------------------------------------------------------
//	Module:  clear_bottom_line
//--------------------------------------------------------------------------
// Purpose: Clear all 11 segments on the LCD bottom line.
//
// Parameters:	None
//
// Returns:    None
//
//--------------------------------------------------------------------------
#separate
void clear_bottom_line(void)
{
   lcd_send_byte(0,6);              // Auto-increment cursor
   lcd_gotoxy(1,2);                 // Go to 2nd line, 1st position
   lcd_putc("           ");         // Clear 11 LCD segments
   delay_ms(1);                     // from 1st to 11th
}

//--------------------------------------------------------------------------
//	Module:  display_bottom_line
//--------------------------------------------------------------------------
// Purpose: Display the marker on the LCD bottom line to represent the input
//          voltage.
//
// Parameters:	None
//
// Returns:    None
//
//--------------------------------------------------------------------------
#separate
void display_bottom_line(void)
{
   vin = read_ADC();          // Take ADC reading

   find_column();             // Find LCD column on 2nd line

   display_marker();          // Display marker position on LCD 2nd line
}


//--------------------------------------------------------------------------
//	Module:  display_main_top_line
//--------------------------------------------------------------------------
// Purpose: Display LCD top line when the input voltage is being displayed on
//          the LCD bottom line.
//          WARNING: DO NOT ADJUST SPACING ON TOP LINE, AS THIS WILL AFFECT THE
//          POSITION OF THE MAX MARKER POSITION.
//
// Parameters:	None
//
// Returns:    None
//
//--------------------------------------------------------------------------
#separate
void display_main_top_line(void)
{
   // Display 1st line normal display
   lcd_putc("\f>>-INC-");  // Message to tell user of weight increase direction.
   lcd_putc(R2LINE);       // This marker represents 2.0V
   lcd_putc(" -WT-->");
}



//--------------------------------------------------------------------------
//	Module:  Check battery voltage (12V version)
//--------------------------------------------------------------------------
// Purpose: Check voltage on rechargeable battery on power up and display
//          remaining charge in percentage.
//
// Parameters:	None
//
// Returns:    None
//
//--------------------------------------------------------------------------
#separate
void check_bat(void)
{
   int8 charge;

   restart_wdt();                   // Reset watchdog to at least 1.5s
   lcd_putc("\fBAT CHARGE LEFT");

   set_adc_channel(1);              // Select battery channel
   delay_ms(1);
   vin = read_ADC();                // Take ADC reading

   if(vin<=659) // 659bits = 3.22V
   {
      charge=0;
   }
   else if(vin>=1003) // 1003bits = 4.90V
   {
      charge=100;
   }
   else if(vin>659)
   {
      // charge=((vin-659)*100)/(1003-659);
      charge=(vin-659)*0.29069;
   }

   printf(lcd_putc,"\n= %4u%%", charge);
   delay_check_OK_PB(4);      // Delay 2s
}


/*
//--------------------------------------------------------------------------
//	Module:  Check battery voltage (9V version)
//--------------------------------------------------------------------------
// Purpose: Check voltage on 9V PP3 battery.
//
// Parameters:	None
//
// Returns:    None
//
//--------------------------------------------------------------------------
#separate
void check_bat(void)
{
   float anal_vin=0.0;

   set_adc_channel(1);              // Select battery channel
   delay_ms(1);
   vin = read_ADC();                // Take ADC reading

   // Includes diode drop and 150k/100k resistor network
   // Vdiode = 0.74V in prototype.
   // VBat  diode        RNet   Vin
   // 8.89V -----> 8.15V ----> 3.23V (661)

   anal_vin = (float)(vin*5)/1024;
   anal_vin /= 0.39632;
   anal_vin += 0.74;

   printf(lcd_putc,"\fVBat = %3.1fV", anal_vin);

   if(anal_vin<8.0)
      printf(lcd_putc,"\nReplace Battery!");
   else if(anal_vin>=8.0 && anal_vin<8.3)
      printf(lcd_putc,"\nLow Voltage!");

   delay_ms(2000);                // Wait 2s
}
*/
