/*****************************************************************************
      Red Forge Ltd
      9, Palmers Road
      Moons Moat East
      Redditch
      Worcs
      England
      B98 0RF

      Title:          LCD source file for Tipper Display Software
      Filename:       lcd1.c
      Issue:          V2.02
      Written by:     Tommy Liu
      Modified by:    Tommy Liu
      Date:           03 Jun 2008
      Compiler:       CCS PCWH V3.249

      Red Forge Ltd owns the copyright in this document and associated
      documents and all rights are reserved.  This document and associated
      documents must not be used for any purpose other than that for which
      they are supplied and must not be copied in whole or in part, or
      disclosed to others without the prior written consent of Red Forge Ltd.
      Any copy of this document made by any method must also contain a copy
      of this legend.

               (c) Red Forge Ltd. 2008

--------------------------------------------------------------------------------

      CHANGE HISTORY
      Issue   Author      Date
      1.00    T Liu       Mar 2008
      Prototype

      2.00    T Liu       28 Apr 2008
      Production

      2.02    T Liu       03 Jun 2008
      - Increased delay from 15ms to 50ms in LCD INIT routine for all LCD's
      in case strange chars appearing. Increased other delays (commented) in
      various LCD routines as well.
      2.1   Chris Clews August 2012
            Minor addition for Weststar OLED LCD.
            In lcd_putc() when handling '\f'=0x0C, added lcd_gotoxy(1,1) to ensure home cursor

*******************************************************************************/

////////////////////////////////////////////////////////////////////////////
////                             lcd1.C                                 ////
////                 Driver for common LCD modules                      ////
////                                                                    ////
////  lcd_init()   Must be called before any other function.            ////
////                                                                    ////
////  lcd_putc(c)  Will display c on the next position of the LCD.      ////
////                     The following have special meaning:            ////
////                      \f  Clear display                             ////
////                      \n  Go to start of second line                ////
////                      \b  Move back one position                    ////
////                                                                    ////
////  lcd_gotoxy(x,y) Set write position on LCD (upper left is 1,1)     ////
////                                                                    ////
////  lcd_getc(x,y)   Returns character at position x,y on LCD          ////
////                                                                    ////
////////////////////////////////////////////////////////////////////////////
////        (C) Copyright 1996,1997 Custom Computer Services            ////
//// This source code may only be used by licensed users of the CCS C   ////
//// compiler.  This source code may only be distributed to other       ////
//// licensed users of the CCS C compiler.  No other use, reproduction  ////
//// or distribution is permitted without written permission.           ////
//// Derivative programs created using this software in object code     ////
//// form are not restricted in any way.                                ////
////////////////////////////////////////////////////////////////////////////
/* 
struct lcd_pin_map {                 // This structure is overlayed
           boolean rs;               // access to the LCD pins.
           boolean enable;           // on to an I/O port to gain
           int     data : 4;
        } lcd;
 */
struct lcd_pin_map {                 // This structure is overlayed
           int     data : 4;
           boolean enable;           // on to an I/O port to gain
           boolean rs;               // access to the LCD pins.
        } lcd;


#byte lcd = port_c          // Onto port C

#define lcd_type 2           // 0=5x7, 1=5x10, 2=2 lines
#define lcd_line_two 0x40    // LCD RAM address for the second line


byte CONST LCD_INIT_STRING[4] = {0x20 | (lcd_type << 2), 0xc, 1, 6};
                             // These bytes need to be sent to the LCD
                             // to start it up.


void lcd_send_nibble( byte n ) {
      lcd.data = n;
      delay_cycles(5);   // Was 1.  Required for Varitronix LCD (Samsung).
      lcd.enable = 1;
      delay_us(5);       // Was 2.  Required for Varitronix LCD (Samsung).
      lcd.enable = 0;
}


void lcd_send_byte( byte address, byte n ) {

      restart_wdt();    // Reset watchdog to 2.3s +/- 50%
      lcd.rs = 0;
      lcd.rs = address;
      delay_cycles(5);  // Was 1.  Required for Varitronix LCD (Samsung).
      delay_cycles(5);  // Was 1.  Required for Varitronix LCD (Samsung).
      lcd.enable = 0;
      lcd_send_nibble(n >> 4);
      lcd_send_nibble(n & 0xf);
}


void lcd_init() {
    byte i;

    restart_wdt();    // Reset watchdog to 2.3s +/- 50%

    lcd.rs = 0;
    lcd.enable = 0;

    // Changed to 50ms (was 15ms) delay for Samsung driver chip used on
    // Varitronix LCD's in order to overcome strange chars appearing.
    delay_ms(50);
    for(i=1;i<=3;++i) {
       lcd_send_nibble(3);
       delay_ms(10);      // Was 5.  Required for Varitronix LCD (Samsung).
    }
    lcd_send_nibble(2);
    for(i=0;i<=3;++i)
       lcd_send_byte(0,LCD_INIT_STRING[i]);
}


void lcd_gotoxy( byte x, byte y) {
   byte address;

   if(y!=1)
     address=lcd_line_two;
   else
     address=0;
   address+=x-1;
   lcd_send_byte(0,0x80|address);
}

void lcd_putc( char c) {
   switch (c) {
     case '\f'   : lcd_send_byte(0,1);
                   delay_ms(2);
                   lcd_gotoxy(1,1); // force to start of 1st row. This is for MSV Winstar "oled" lcd
                                           break;
     case '\n'   : lcd_gotoxy(1,2);        break;
     case '\b'   : lcd_send_byte(0,0x10);  break;
     default     : lcd_send_byte(1,c);     break;
   }
}

//--------------------------------------------------------------------------
//   Module:  create_CGRAM_chars
//--------------------------------------------------------------------------
// Purpose: Create 2 custom LCD characters. Upto 8 characters can be created.
//
// Parameters:   None
//
// Returns:    None
//
//--------------------------------------------------------------------------
#separate
void create_CGRAM_chars(void)
{
   int i;

   restart_wdt();    // Reset watchdog to 2.3s +/- 50%

   lcd_send_byte(0, 0x40);          // Set CGRAM address to 0
   for(i=0; i<8; i++)
   {
      delay_us(200);
      lcd_send_byte(1, alldots[i]);
   }

   lcd_send_byte(0, 0x40+8);          // Set CGRAM address to 0+8
   for(i=0; i<8; i++)
   {
      lcd_send_byte(1, mute_speaker[i]);
   }
}
