/*****************************************************************************
      Red Forge Ltd Palmers Road
      Palmers Road
      Moons Moat East
      Redditch
      Worcs
      England
      B98 0RF

      Title:          Header file for PGU/RCU
      Filename:       header.h
      Issue:          V2.0
      Written by:     Tommy Liu
      Modified by:    Tommy Liu
      Date:           11 Mar 2010
      Compiler:       CCS PCWH V3.249

      Red Forge Ltd owns the copyright in this document and associated
      documents and all rights are reserved.  This document and associated
      documents must not be used for any purpose other than that for which
      they are supplied and must not be copied in whole or in part, or
      disclosed to others without the prior written consent of Red Forge Ltd.
      Any copy of this document made by any method must also contain a copy
      of this legend.

               (c) Red Forge Ltd. 2010

--------------------------------------------------------------------------------

      CHANGE HISTORY
      Issue   Author      Date

      1.0     T Liu	     16 Oct 2008
      First Issue.

      2.0     T Liu	     11 Mar 2010
      - Added defines and prototypes for PGUPLUS.

*******************************************************************************/

#include <18F2620.h>             // Written by CCS
//#device *=16                   // Use 16-bit pointer to make full use of all
                                 // RAM locations (redundant for 18F252)
#device ADC=10                   // Select 10-bit ADC (0 to 1023)
                                 // with 5V precision reference source

#FUSES NOWDT                    // Watchdog disabled
#FUSES WDT128                   // Watch Dog Timer uses 1:128 Postscale
#FUSES XT                       //Crystal osc <= 4mhz
#FUSES PROTECT                  // Code protected from reading
#FUSES NOIESO                   //Internal External Switch Over mode disabled
#FUSES BROWNOUT                 //Reset when brownout detected
#FUSES BORV45                   //Brownout reset at 4.5V
#FUSES PUT                      // Power Up Timer
#FUSES NOCPD                    //No EE protection
#FUSES STVREN                   //Stack full/underflow will cause reset
#FUSES NODEBUG                  // No Debug mode for ICD
#FUSES NOLVP                    //No Low Voltage Programming on B3(PIC16) or B5(PIC18)
#FUSES NOWRT                    //Program memory not write protected
#FUSES NOWRTD                   //Data EEPROM not write protected
#FUSES NOEBTR                   //Memory not protected from table reads
#FUSES NOCPB                    //No Boot Block code protection
#FUSES NOEBTRB                  //Boot block not protected from table reads
#FUSES NOWRTC                   //configuration not registers write protected
#FUSES NOWRTB                   //Boot block not write protected
#FUSES NOFCMEN                  //Fail-safe clock monitor disabled
#FUSES NOXINST                  //Extended set extension and Indexed Addressing mode disabled (Legacy mode)
#FUSES PBADEN                    //PORTB pins are configured as analog input channels on RESET
#FUSES NOLPT1OSC                //Timer1 configured for higher power operation
#FUSES MCLR                     //Master Clear pin enabled

// 4MHz clock (internal clock of MCU=1MHz)
#use     delay(clock=4000000)

#define IR_IN           PIN_B0   // Input  (Infrared module input)
#define SOUNDER         PIN_B1   // Output (Piezo sounder)
#define SELECT_SW       PIN_B2   // Input  (PGU/RCU toggle swtch)

#define PGU             1        // Selections on toggle switch
#define RCU             0

#define ENABLED         1
#define DISABLED        0

#define HIGH            1
#define LOW             0

#define STAY            1
#define LEAVE           0

#define FLINE           0  // Marker with 3 lines in LCD segment
#define L2LINE          1  // Single line at end left in LCD segment
#define L1LINE          2  // Single line offset just to left in LCD segment
#define CLINE           3  // Single centre line in LCD segment
#define R1LINE          4  // Single line offset just to right in LCD segment
#define R2LINE          5  // Single line at end right in LCD segment


// Timer0 value
#define T50msValue               61  // 256 - (0.05/(4/(4000000/256))

// Timer1 values
#define T100msValue           53036  // 65536 - (0.10/(4/(4000000/8))
#define T500msValue            3036  // 65536 - (0.50/(4/(4000000/8))

#define DIV_5VBY1024          4.8828E-3   // = (5.0V/1024)

#define MAX(A,B)              ((A>B)?A:B)
#define MIN(A,B)              ((A<B)?A:B)

#define Y_PRESSED       10
#define N_PRESSED       20
#define ONE_PRESSED     30
#define TWO_PRESSED     40

#define CORRECT         15
#define WRONG           16
#define TOO_MANY_PINS   17

#define INITIALNUMPINS  50
#define THRESHNUMPINS   25
#define MAXNUMPINS      75
#define MAXNUMSTONES    5
#define NUMPINS_ADDR    0x00
#define NUMSTONES_ADDR  0x02

#define TOTALNUMSTONESUSED_HI_ADDR  0x10
#define TOTALNUMSTONESUSED_MD_ADDR  0x11
#define TOTALNUMSTONESUSED_ND_ADDR  0x12
#define TOTALNUMSTONESUSED_LO_ADDR  0x13

#define TOTALNUMPINSUSED_HI_ADDR    0x15
#define TOTALNUMPINSUSED_MD_ADDR    0x16
#define TOTALNUMPINSUSED_ND_ADDR    0x17
#define TOTALNUMPINSUSED_LO_ADDR    0x18

#define TEN_SEC_TIMEOUT 20

// For IR routines
typedef unsigned int8   U8;
typedef signed int8     S8;
typedef unsigned int16  U16;
typedef signed int16    S16;
typedef unsigned int32  U32;
typedef signed int32    S32;
typedef int1            bool;



// FUNCTION PROTOTYPES
// For PGU
#int_TIMER3
void Timer3_ISR(void);
#separate
void setup_ports(void);
#separate
void configure_ADC_ports(void);
#separate
void write_defaults (void);
#separate
void read_eeprom_data(void);
#separate
void splitU32Into4 (U32 *ptrIn, U8 *ptrHi, U8 *ptrMd, U8 *ptrNd, U8 *ptrLo);
#separate
U32 join4bytes (U8 hi, U8 md, U8 nd, U8 lo);
#separate
U8 checkPINsEnterStone (void);
#separate
void checkStones(void);
#separate
void displayData(void);
#separate
void display_NOMORESTONES_msg (void);
#separate
int32 enter_AC (void);
#separate
U8 enterStoneAndDisplayAC (void);
#separate
void enterSeedAndDisplayPIN (void);
U16 rand_RF(void);
void srand_RF(U32 seed_RF);
#separate
U32 generate_seed (void);
#separate
U32 generate_PIN (U32 seed_value);
#separate
U32 generate_stone (void);
#separate
U32 generate_AC (U32 stone_value);
#separate
U32 enter_seed (void);
#separate
void pgu_function (void);
#separate
void delay_check_OK_PB(U8 dly);
#separate
void delay_check_12_PB (U8 dly);
#separate
void delay_check_2_PB (U8 dly);
#separate
void delay_check_F1_PB (U8 dly);


// For RCU
#separate
void rcu_function (void);
#int_TIMER0
void SOUNDER_ISR(void);
#separate
void enable_sounder (void);
#separate
void disable_sounder (void);
#separate
void find_column(void);
#separate
void find_column(void);
#separate
void display_voltage(void);
#separate
void display_marker(void);
#separate
void clear_bottom_line(void);
#separate
void display_bottom_line(void);
#separate
void display_main_top_line(void);
#separate
void check_bat(void);
