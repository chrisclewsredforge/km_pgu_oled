/*****************************************************************************
      Red Forge Ltd
      9, Palmers Road
      Moons Moat East
      Redditch
      Worcs
      England
      B98 0RF

      Title:          PGU Plus routines
      Filename:       delays.c
      Issue:          V2.0
      Written by:     Tommy Liu
      Modified by:
      Date:           18 Mar 2010
      Compiler:       CCS PCWH V3.249

      Red Forge Ltd owns the copyright in this document and associated
      documents and all rights are reserved.  This document and associated
      documents must not be used for any purpose other than that for which
      they are supplied and must not be copied in whole or in part, or
      disclosed to others without the prior written consent of Red Forge Ltd.
      Any copy of this document made by any method must also contain a copy
      of this legend.

               (c) Red Forge Ltd. 2010

--------------------------------------------------------------------------------

      CHANGE HISTORY
      Issue   Author      Date

      2.0     T Liu	     18 Mar 2010
      First Issue.

*******************************************************************************/

//--------------------------------------------------------------------------
// Module:  delay_check_OK_PB
//--------------------------------------------------------------------------
// Purpose:    Delay and check OK/YES push button
//             Note: Timer0 interrupts every 0.5s.
//             Using Timer0 ensures that the OK press is picked up everytime.
//
// Parameters: dly in number of half seconds
//             eg. 0.5s = 1
//             eg. 1.0s = 2
//             eg. 1.5s = 3
//
// Returns:    None
//
//--------------------------------------------------------------------------
#separate
void delay_check_OK_PB(U8 dly)
{
   PB_OK_YES_flag=FALSE;      // Reset flag
   set_timer3(T500msValue);   // Reload Timer3
   timer3_overflow_startup=0; // Ensure Timer3 count from beginning
   while(1)
   {
      restart_wdt();    // Reset watchdog to 2.3s +/- 50%

      // Wait until a button is pressed OR dly timeout...
      while( (pb=CheckIRinput())==NOCMD && timer3_overflow_startup<dly );

      // If OK/YES push button pressed, exit while loop immediately...
      if(pb==PB_OK_YES)
      {
         PB_OK_YES_flag=TRUE;
         break;      // Exit while loop
      }

      // If delay is up, exit while loop immediately...
      if( timer3_overflow_startup>=dly )
      {
         timer3_overflow_startup=0;
         break;      // Exit while loop
      }
   }
}


//--------------------------------------------------------------------------
//   Module:  delay_check_12_PB
//--------------------------------------------------------------------------
// Purpose:    Delay X seconds and check 1 and 2 push buttons
//
// Parameters: seconds
//
// Returns:    None
//
//--------------------------------------------------------------------------
#separate
void delay_check_12_PB (U8 dly)
{
   U16 timeout=0;

   // IMPORTANT: Do NOT reduce delay_ms(20) and increase (dly*50),
   // otherwise for long delays eg. 4s, it does not work!
   PB_1_flag=FALSE;
   PB_2_flag=FALSE;
   while( timeout <= (dly*50) )
   {
      delay_ms(20);
      // If 1 push button pressed, exit immediately
      pb=CheckIRinput();
      if(pb==PB_1)
      {
         PB_1_flag=TRUE;
         return;
      }
      // If 2 push button pressed, exit immediately
      if(pb==PB_2)
      {
         PB_2_flag=TRUE;
         return;
      }
      timeout++;
   }
}


//--------------------------------------------------------------------------
//   Module:  delay_check_2_PB
//--------------------------------------------------------------------------
// Purpose:    Delay X seconds and check 2 push button
//
// Parameters: seconds
//
// Returns:    None
//
//--------------------------------------------------------------------------
#separate
void delay_check_2_PB (U8 dly)
{
   U16 timeout=0;

   // IMPORTANT: Do NOT reduce delay_ms(20) and increase (dly*50),
   // otherwise for long delays eg. 4s, it does not work!
   PB_2_flag=FALSE;
   while( timeout <= (dly*50) )
   {
      delay_ms(20);
      // If OK/YES push button pressed, exit immediately
      pb=CheckIRinput();

      // If 2 push button pressed, exit immediately
      if(pb==PB_2)
      {
         PB_2_flag=TRUE;
         return;
      }
      timeout++;
   }
}


//--------------------------------------------------------------------------
//   Module:  delay_check_F2_PB
//--------------------------------------------------------------------------
// Purpose:    Delay X seconds and check F2 push button
//
// Parameters: seconds
//
// Returns:    None
//
//--------------------------------------------------------------------------
#separate
void delay_check_F2_PB (U8 dly)
{
   U16 timeout=0;

   // IMPORTANT: Do NOT reduce delay_ms(20) and increase (dly*50),
   // otherwise for long delays eg. 4s, it does not work!
   PB_F2_flag=FALSE;
   while( timeout <= (dly*50) )
   {
      delay_ms(20);
      // If OK/YES push button pressed, exit immediately
      pb=CheckIRinput();

      // If F2 push button pressed, exit immediately
      if(pb==PB_F2)
      {
         PB_F2_flag=TRUE;
         return;
      }
      timeout++;
   }
}

